﻿using Desafio_Meta3.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Desafio_Meta3.WebUI.Controllers
{
    public class FuncionarioController : Controller
    {
        // GET: Funcionario
        public ActionResult Index(string sortOrder, string searchString)
        {
            FuncionarioViewModel usuarioViewModel = new FuncionarioViewModel
            {
                FuncionarioLista = Negocio.ControleFuncionario.ObterListaFuncionario()
            };

            return View(usuarioViewModel.FuncionarioLista);
        }

        // GET: Funcionario/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Funcionario/Create
        public ActionResult Create()
        {
            FuncionarioViewModel funcionarioViewModel = new FuncionarioViewModel
            {
                PessoaFisicaLista = Negocio.ControlePessoaFisica.ObterListaPessoaFisica(),
                FuncaoLista = Negocio.ControleFuncionario.ObterListaFuncao()
            };

            ViewBag.Pessoas = funcionarioViewModel.PessoaFisicaLista;
            ViewBag.Funcoes = funcionarioViewModel.FuncaoLista;

            return View();
        }

        // POST: Funcionario/Create
        [HttpPost]
        public ActionResult Create(Modelo.Funcionario funcionario)
        {
            try
            {
                FuncionarioViewModel funcionarioViewModel = new FuncionarioViewModel
                {
                    PessoaFisicaLista = Negocio.ControlePessoaFisica.ObterListaPessoaFisica(),
                    FuncaoLista = Negocio.ControleFuncionario.ObterListaFuncao()
                };

                ViewBag.Pessoas = funcionarioViewModel.PessoaFisicaLista;
                ViewBag.Funcoes = funcionarioViewModel.FuncaoLista;
                ViewBag.sucesso = "Registro salvo com sucesso";
                return View(funcionario);
            }
            catch(Exception e)
            {
                ViewBag.error = e.Message;
                return View();
            }
        }

        // GET: Funcionario/Edit/5
        public ActionResult Edit(int id)
        {
            FuncionarioViewModel funcionarioViewModel = new FuncionarioViewModel
            {
                PessoaFisicaLista = Negocio.ControlePessoaFisica.ObterListaPessoaFisica(),
                FuncaoLista = Negocio.ControleFuncionario.ObterListaFuncao(),
                Funcionario = Negocio.ControleFuncionario.ObterFuncionarioPorId(id)
            };

            ViewBag.Pessoas = funcionarioViewModel.PessoaFisicaLista;
            ViewBag.Funcoes = funcionarioViewModel.FuncaoLista;

            return View(funcionarioViewModel.Funcionario);
        }

        // POST: Funcionario/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Modelo.Funcionario funcionario)
        {
            try
            {
                FuncionarioViewModel funcionarioViewModel = new FuncionarioViewModel
                {
                    PessoaFisicaLista = Negocio.ControlePessoaFisica.ObterListaPessoaFisica(),
                    FuncaoLista = Negocio.ControleFuncionario.ObterListaFuncao(),
                    
                };

                ViewBag.Pessoas = funcionarioViewModel.PessoaFisicaLista;
                ViewBag.Funcoes = funcionarioViewModel.FuncaoLista;
                ViewBag.sucesso = "Registro salvo com sucesso";
                return View(funcionario);
            }
            catch(Exception e)
            {
                ViewBag.error = e.Message;
                return View();
            }
        }

        // GET: Funcionario/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Funcionario/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Negocio.ControleFuncionario.DeletarFuncionario(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
