﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desafio_Meta3.WebUI.Models
{
    public class PessoaFisicaViewModel
    {
        public List<Modelo.PessoaFisica> PessoaFisicaLista { get; set; }
        public Modelo.PessoaFisica PessoaFisica { get; set; }
    }
}