﻿using Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Desafio_Meta3.WebUI.Models
{
    public class UsuarioViewModel
    {
        public List<Usuario> UsuarioLista { get; set; }
        public Usuario Usuario { get; set; }
    }
}