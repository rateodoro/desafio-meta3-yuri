﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositorio.Scripts
{
    public class Funcionario
    {
        public const string ObterListaFuncionarios = @"SELECT	f.Id,
                                                                f.IdPessoaFisica,
                                                                f.IdFuncao,
                                                                f.Salario,
                                                                f.DataAdminissao,
                                                                f.DataDemissao,
                                                                f.Ativo
                                                            FROM Funcionario f";

        public const string InserirFuncionario = @"INSERT INTO Funcionario (IdPessoaFisica, IdFuncao, Salario, DataAdminissao, DataDemissao, Ativo)
                                                    VALUES (@IdPessoaFisica, @IdFuncao, @Salario, @DataAdmissao, @DataDemissao, @Ativo )";

        public const string AtualizarFuncionario = @"UPDATE Funcionario set 
                                                        IdPessoaFisica = @IdPessoaFisica,
                                                        IdFuncao = @IdFuncao,
                                                        Salario = @Salario,
                                                        DataAdminissao = @DataAdmissao,
                                                        DataDemissao = @DataDemissao,
                                                        Ativo = @Ativo
                                                        FROM Funcionario
                                                        WHERE Id = @IdFuncionario";

        public const string DeletarFuncionario = @"DELETE FROM Funcionario WHERE Id = @IdFuncionario ";

        public const string ObterFuncionarioPorId = @"SELECT	f.Id,
                                                                f.IdPessoaFisica,
                                                                f.IdFuncao,
                                                                f.Salario,
                                                                f.DataAdminissao,
                                                                f.DataDemissao,
                                                                f.Ativo
                                                            FROM Funcionario f
                                                    WHERE f.Id = @IdFuncionario";

        public const string ObterListaFuncao = @"SELECT	Id,
                                                                Nome
                                                                FROM Funcao";

    }
}
